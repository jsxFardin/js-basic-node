let obj1 = {
    name : 'fardin',
    age : 21
}

let obj2 = obj1 ;

let obj3 = obj2;

obj2.age = 23;

obj1.age1 = 50;

console.log(obj1); // fardin
console.log(obj2); // majed
console.log(obj3); // majed

let a = 10;
let b = a;