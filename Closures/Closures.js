let counter = (() => {
    let privateCounter = 0;

    function changeVal(val) {
        privateCounter += val;
    }

    return {
        increment: () => {
            changeVal(1);
        },
        decrement: () => {
            changeVal(-1);
        },
        value: () =>{
            return privateCounter;
        }
    };

})();

// console.log(counter.value());
// counter.increment();
// console.log(counter.value());

function counters(){
    let privateCounter = 0;

    function changeVal(val) {
        privateCounter += val;
    }

    return {
        increment: () => {
            changeVal(1);
        },
        decrement: () => {
            changeVal(-1);
        },
        value: () =>{
            return privateCounter;
        }
    };
}

let c = counters();
// console.log(c.value());
c.increment();
console.log(c.value());
