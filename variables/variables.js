/*
    key word for variablle
    1. var
    2. let (es-6)
    3. const (es-6)

    syntex for variabel declaration
    1.let variablleName
    2.var variablleName
    3.const variablleName
*/
//  declare  a variale 
var variablleName ;
let variablleName1 ;
// const variablleName2 ; 

// assign a value 
variablleName = "hello world";
variablleName1 = 10;
variablleName2 =40;

// declaration and assign value together
let a = 10;
var b = "fardin";
const pi = 3.1416;

// declaration and assign value with out keyword
number = 10;

console.log(number);



