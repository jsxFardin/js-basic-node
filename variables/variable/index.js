/* 
    // primitive type
    sting
    number
    boolean
    undefined
    null
 */
let name = "fardin"; // String literal
let age = 21;
let isApproved = false;
let firstName = undefined;
let lastName = null;

/* 
    // reference type
    object 
    array
    function
*/

let person = {
    name : "fardin",
    age : 21
}

console.log(person.name)
 