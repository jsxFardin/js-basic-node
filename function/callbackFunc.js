let date = new Date();
let currentYear = date.getFullYear();

let years = [1990, 2000, 1998, 1997];


function arrayCal(arr, callBackFunc) {
    let data = [];

    for (let i = 0; i < arr.length; i++) {
        data.push(callBackFunc(arr[i]));  // here callBackFunction is callBackFunc
    }
    return data;
}

function calAge(el) {
    return currentYear - el;
}

let ages = arrayCal(years, calAge);

console.log(ages);