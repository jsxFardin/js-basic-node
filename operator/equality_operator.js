// JS Nuggets: == vs ===

// == abstract equality

// === strict equality

//  "this is string"  => string literals
// new String("This is a string.") => string object

console.log(3 == "3");
console.log(3 === "3");

console.log(true == '1');
console.log(true === '1');

console.log("This is a string." == new String("This is a string."));
console.log("This is a string." === new String("This is a string."));