let number1 = document.getElementById("number1");
let number2 = document.getElementById("number2");
let result = document.getElementById("result");

let simpleCalculator = {
    add: () => {
        result.value = parseInt(number1.value) + parseInt(number2.value);
    },
    substract : () => {
        result.value = parseInt(number1.value) - parseInt(number2.value);
    },
    multipication : () => {
        result.value = parseInt(number1.value) * parseInt(number2.value);
    },
    division : () => {
        result.value = parseInt(number1.value) / parseInt(number2.value);
    },
    modulus : () => {
        result.value = parseInt(number1.value) % parseInt(number2.value);
    }

}

// multipication division 

document.getElementById("sum").addEventListener('click', simpleCalculator.add, false);
document.getElementById("sub").addEventListener('click', simpleCalculator.substract, false);
document.getElementById("multipication").addEventListener('click', simpleCalculator.multipication, false);
document.getElementById("division").addEventListener('click', simpleCalculator.division, false);
document.getElementById("modulus").addEventListener('click', simpleCalculator.modulus, false);