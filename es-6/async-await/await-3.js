/* 
 *Can’t use await in regular functions

 *We will get this error if we do not put async before a
 *function. As said, await only works inside an async function. 
 
*/

function f() {
    let promise = Promise.resolve(1);
    let result = await promise; // Syntax error

    console.log(result);
}
f();

