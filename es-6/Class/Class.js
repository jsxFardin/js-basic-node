// class declaration
class Rectangle {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
}

// class expretion
let Rectangle = class {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
}
console.log(Rectangle.name);
// output: "Rectangle"