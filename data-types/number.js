
var number = 10;
// or 
number = 10;
// --------------------//

//  add value 
number = number +10;
// or 
number += 10;
/* ------------------- */

// increment 
number ++; //post 
++number; // pre
// or
number = number + 1;
/* ------------------- */

// decrement
number--; // post
-- number; // pre
/* ------------------- */
