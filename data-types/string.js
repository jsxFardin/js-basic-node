/* Strings */

var myName = 'fardin';

var sentence = "He said \"Hi!\""; // He said Hi!
var sentence = 'He said "Hi!"'; // He said Hi!

/*
Code	Output
\'	single quote
\"	double quote
\\	backslash
\n	newline
\r	carriage return
\t	tab
\b	backspace
\f	form feed
*/

var fullName = 'Fardin ' + 'Ahsan';  // fardin ashan

var sentence2 = 'My name is ' + fullName; // 'My name is fardin ahsan'

fullName += ' is my name.'; // 'fardin ahsan is my name'

name = ` my name is ${fullName} `; // template literal es-5/es2015
// console.log(name);

// ?----------------------------------------//
// stirng back